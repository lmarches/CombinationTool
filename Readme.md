# A likelihood combination tool #

__Authors: [Andrea Gabrielli][andrea], [Stefan Gadatsch][stefan]__

*The documentation is written in Markdown, convert to HTML or read it in your favorite viewer.*

## Quick start ##

* [SVN location][svn]
* [README][readme]
* [Sample code][example]

## Introduction ##

The combination tool was developed to combine general probability density functions (PDFs) of arbitrary type, shape and complexity using the RooFit framework. The tool is designed to be modular and thus highly extendable. The resulting PDF is stored in a RooWorkspace which can be persisted and used from the RooStats high-level statistical tools like e.g. the AsymptoticCalculator, which is widely adopted by the LHC experiments. In particular, this means the same tools can be used with the combined model and the input models.

Key to the combination of likelihoods is the development of a correlation scheme, possibly supporting soft correlations between (nuisance) parameters. Hence, emphasize is put on providing a simple interface to input these using standalone (C/C++) applications or scripting language, e.g. Python/PyROOT. Beyond this, the structure of the different input models will be regularized to have a PDF as universal as possible.

Initially the tool was derived from a framework from [Aaron Armbruster][aaron], but re-written from scratch with usability and simplicity in mind, but on the other hand keeping generality and making it usable for a wide range of problems. It was written originally to serve as an independent cross-check of a framework by [Haoshuang Ji][haoshuang], available on [SVN][svn_oldtool], and used for the ATLAS Higgs combination before. However, the new tool is intended to serve as a general framework to perform any likelihood combination, based on RooFit/RooStats models, not only inside the Higgs working group, but also beyond.

### Design goals ###

* Based on RooFit/RooStats packages
* Ability to handle arbitrarily complex models
* Ability to combine multiple measurements and correlate the parameters across them
* Ability to incorporate soft correlations
* Ability to determine parameters to correlate automatically
* Ability to regularize multiple channels
* Modular and extendable design
* Ability to re-parametrise models, including re-casting constraint types
* Natural and intuitive user interface

Hence, the combination tool consists of various RooFit based classes, similar to the HistFactory and HistFitter approach.

A CombinedMeasurement holds a set of Measurements supposed to be combined. Correlations between these Measurements are described using a CorrelationScheme. The CorrelationScheme stores re-namings of parameters in a RenamingMap per Measurement. Each Measurement consists of Channels that can be regularized according to a RenamingMap. A CombinedMeasurement can be re-parametrised according to a ParametrisationSequence of ParametrisationSchemes.

### User base ###

The user base includes for example

* LHC Higgs combination group
* SUSY HistFitter community
* ATLAS mass and coupling measurements
* ATLAS light Higgs BSM interpretations
* CMS and LHCb B\_s -> mumu combination

## Availability and setup ##

The code is available on [SVN][svn] and includes a [README][readme] and [sample code][example] with additional instructions to build a complex combined model of the Moriond 2013 H -> WW -> lvlv analysis. To check it out:

    svn checkout svn+ssh://$USER@svn.cern.ch/reps/atlasoff/PhysicsAnalysis/HiggsPhys/CombinationTools/RooStatTools/trunk/CombinationTool CombinationTool

The library comes with a Makefile and uses platform depended compiler flags from ROOT. To compile the shared library, setup gcc, python and ROOT, go to the main folder of the library and build the tool:

    cd CombinationTool && make

The shared library will be compiled into lib/libCombinationTool.so and is ready to be used in applications or scripts, e.g. within a PyROOT script like

    import ROOT
    from ROOT import gSystem
    gSystem.Load("lib/libCombinationTool.so")

To clean the installation, use `make clean` or `make distclean`.

### Requirements ###

The combination framework makes use of some of the latest features of RooFit. Thus it is recommended to use the current ROOT v5-34-00-patches release or the ATLAS HSG7 ROOT build as described in the [TWiki][hsg7root].

The current patches release can be cloned and compiled like

    git clone http://root.cern.ch/git/root.git
    cd root
    git checkout -t origin/v5-34-00-patches
    ./configure
    make -j4

Alternatively, the pre-compiled ATLAS HSG7 ROOT build can be setup using

    ./afs/cern.ch/atlas/project/HSG7/root/current/setup.sh

Of course it is possible to setup a ROOT release from CVMFS and thus should be applicable on a wide range of systems, including, but not only, lxplus.

    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
	setupATLAS
    localSetupROOT <value>

## Examples and usage ##

The library is shipped with two sample usages:

- __Python based script, using the shared library__
- Standalone cpp application

In the example a 7 TeV H -> WW -> lvlv workspace will be combined with its 8 TeV counterpart. The example illustrates how to rename constrained as well as unconstrained nuisance parameters without correlating them and also shows how to correlate parameters between different measurements. Additionally, the optional use of the post-combination re-parametrisation facility is shown and Asimov data is generated. In the following the steps will be explained one at a time for the Python based script. Note that a ROOT macro or standalone application work identically.

### Python based script, alternatively a simple ROOT macro ###

The script can be found in `scripts/combine_lvlv.py`. Extensive comments document every practical step needed for running a likelihood combination. There is no need for complication and the code can be run like

    ./scripts/combine_lvlv.py

The combined workspace is written to workspaces/combined.root. The following steps happen during the process:

#### Loading of the shared library ####

As mentioned earlier, the shared library is loaded as

    import ROOT
    from ROOT import gSystem
    gSystem.Load("lib/libCombinationTool.so")

#### Definition of CombinedMeasurement ####

A CombinedMeasurement with name combined_master is initialized like

    combined = ROOT.CombinedMeasurement("combined_master")

#### Definition of input measurements to combine ####

In the following lines a Measurement with name lvlv2012 is created. The nominal/initial parameter values of this model are stored in the snapshot nominalNuis. It will be loaded from the RooWorkspace combined, persisted in the file `workspaces/lvlv_8TeV_125.5.root`. The model is specified in ModelConfig and the dataset associated with this Measurement is called obsData. Finally it is added to the CombinedMeasurement.

    lvlv2012 = ROOT.Measurement("lvlv2012")
    lvlv2012.SetSnapshotName("nominalNuis")
    lvlv2012.SetFileName("workspaces/lvlv_8TeV_125.5.root")
    lvlv2012.SetWorkspaceName("combined")
    lvlv2012.SetModelConfigName("ModelConfig")
    lvlv2012.SetDataName("obsData")
    combined.AddMeasurement(lvlv2012)

All inputs to the combination have to be initialized and added like this.

#### Definition of a correlation scheme ####

Key to the combination is the definition of a correlation scheme:

    correlation = ROOT.CorrelationScheme("CorrelationScheme")

By default all discrimination variables (observables) and physics PDFs are de-correlated between Measurements. Parameters of interest in this scheme are declared as follows. All parameters in that list, if present in the final combination, will be defined as parameter of interest in the ModelConfig of the output measurement.

    correlation.SetParametersOfInterest("mu,mu_XS7_ggF,mu_XS8_ggF,mu_XS7_VBF,mu_XS8_VBF,mu_XS7_WH,mu_XS8_WH,mu_XS7_ZH,mu_XS8_ZH,mu_BR_WW,mu_BR_tautau")

Parameters between different the various measurements are correlated and also renamed like

    correlation.CorrelateParameter("lvlv2012::SigXsecOverSM_HWW,lvlv2012::SigXsecOverSM_Htt,lvlv2012::SigXsecOverSM,lvlv2011::SigXsecOverSM_HWW", "mu")
    correlation.CorrelateParameter("lvlv2011::alpha_ATLAS_BR_VV,lvlv2012::alpha_ATLAS_BR_VV", "ATLAS_BR_VV")

The syntax is inspired by the RooFit factory language. For example parameter SigXsecOverSM\_HWW from the Measurement lvlv2012 will be correlated with parameter SigXsecOverSM\_HWW from the Measurement lvlv2011. The name of the correlated parameter in the output model will be mu. Note that there is no distinction between constrained and unconstrained parameters necessary. If a parameter should be made with specific initial value and range, the following could be used

    correlation.CorrelateParameter("llll11::mH,llll12::mH,gaga11::mHcomb,gaga12::mHcomb", "mH[125.36,120.0,130.0]")

Similarly a single parameter can be renamed only, and in this example fixed to some value:

    correlation.RenameParameter("lvlv2012", "ATLAS_sampleNorm_ggf125.5", "mu_XS8_ggF[1.0]")

Finally the CombinedMeasurement has to learn about this correlation scheme:

    combined.SetCorrelationScheme(correlation)

#### Running the combination ####

The measurements defined earlier are regularized i.e. the structure of the PDF will be unified, parameters and constraint terms renamed according to a common convention, etc. Then a new simultaneous PDF and dataset is build afterwards.

    combined.CollectMeasurements()
    combined.CombineMeasurements()

#### Post-combination operations ####

Sometimes parametrisations or other operations on the final combined workspace are needed, maybe even on a previously combined model. There is no need to re-run the full combination but instead start from an existing CombinedMeasurement.

Parametrisations can be defined in building blocks which are executed in a sequence afterwards. In the given example the 2012 luminosity is changed to 20.3 fb-1 by pulling the nuisance parameter down by 0.5 sigma without touching the uncertainty:

    corrections = ROOT.ParametrisationScheme("corrections")
    corrections.AddExpression("ATLAS_LUMI_2012_diboson=LinearVar::ATLAS_LUMI_2012_diboson_linear(ATLAS_LUMI_2012[0.0,-5.0,5.0],1.0,-0.5555)")
    corrections.AddExpression("ATLAS_LUMI_2012_dibosonConstraint=Gaussian::ATLAS_LUMI_2012Constraint(ATLAS_LUMI_2012,nom_ATLAS_LUMI_2012[0.0],1.0)")
    corrections.AddExpression("nom_ATLAS_LUMI_2012_diboson=nom_ATLAS_LUMI_2012[0.0]")
    corrections.AddNewGlobalObservable("nom_ATLAS_LUMI_2012")
    corrections.AddNewNuisanceParameters("ATLAS_LUMI_2012")

Here the old parameter ATLAS\_LUMI\_2012\_diboson is replaced with a new object ATLAS\_LUMI\_2012\_diboson\_linear specified in factory language, similarly for the constraint. The usage of factory language makes this facility quite powerful. Finally the new global observable and nuisance parameter need to be defined.

As mentioned, many of these blocks can be combined in a sequenece

    parametrisation = ROOT.ParametrisationSequence("parametrisation")
    parametrisation.AddScheme(corrections)
    parametrisation.SetParametersOfInterest("mu,mu_XS7_ggF,mu_XS8_ggF,mu_XS7_VBF,mu_XS8_VBF,mu_XS7_WH,mu_XS8_WH,mu_XS7_ZH,mu_XS8_ZH,mu_BR_WW,mu_BR_tautau")

and applied to the CombinedMeasurement

    combined.SetParametrisationSequence(parametrisation)
    combined.ParametriseMeasurements()

#### Addition of Asimov data and parameter snapshots ####

Asimov data can be added to the CombinedMeasurement as follows:

    combined.MakeAsimovData(ROOT.kTRUE, ROOT.CombinedMeasurement.ucmles, ROOT.CombinedMeasurement.nominal)

This will profile the nuisance parameters at the MLE values of the first POI and generate Asimov data for nominal POI values. All needed parameter snapshots are added automatically.

If only a snapshot should be added without Asimov data to be generated, the command would for example be

    combinedMakeSnapshots(ROOT.CombinedMeasurement.ucmles, ROOT.kTRUE)

#### Persist CombinedMeasurement and print useful information ####

The CombinedMeasurement is stored as a RooWorkspace in a ROOT file. Useful information like the correlation scheme, re-namings per channel, ... etc. are printed.

    combined.writeToFile("workspaces/combined.root")
    combined.Print()

### Standalone cpp application ###

The example code can be found in bin/combineWS.cxx with the corresponding makefile bin/Makefile.standalone. Extensive comments document every practical step needed for running a likelihood combination. To build the standalone application, run

    cd bin
	make -f Makefile.standalone

A binary, combineWS, will appear in the same folder. Run it with

    ./combineWS

to obtain the combined workspace combWS.root.

## Experimental features ##

### Automatic correlation ###

It is possible to automatically determine correlations between channels by matching parameters by name. This is activated by

    correlation.SetAutoCorrelation(ROOT.kTRUE)

However, it should be used with care as it requires the inputs to follow exactly the same conventions. Currently it is implemented for parameters constrained by a Gaussian or Poisson.

### Channel filtering ###

A subset of Channels from a Measurement, e.g. only 0-jet or only control regions, can be selected using a channel filter, which is a string allowing TRegexp expressions and a limited union ('|') which separates these expressions. Not matching channels will be skipped during the collection and regularisation process.

### Nuisance parameter pruning ###

The combined measurement can become quite complex with more and more input measurements. To keep the fitting time manageable it might be desirable to simplify input measurements and reduce the number of nuisance parameters without changing the results.

The currently implemented solution probes the symmetric Hesse uncertainty and ranks parameters iteratively based on a reduced Hesse matrix derived from a RooFitResult (containing a accurate covariance matrix). In particular the code does:

* Find the parameter which changes the Hesse error on specified POIs the least and removes it from the covariance matrix
* Repeat procedure until only 1 parameter remains or the change is above a specified threshold
* Ranking uses error propagation in case multiple POIs are specified to define order of parameters, final error is computed for weighted average of POIs with the passed weights (default SM Higgs cross-section)
* Filter on the parameter name in order to run the pruning on only a subset of all parameters
* Possibility to specify multiple filters that define sets of parameters that can be pruned in given order, for example first MC statistics, then all others
* Only parameters that change individual POIs below a given threshold are marked for pruning. If any POI passes the threshold, the parameter is kept
* 'auto' triggers the automatic determination of thresholds for pruning based on th PDG rules for rounding
* Option to keep more than just the significant digits (default is one more)
* If only one weight or threshold is specified, it will be used for all POIs
* Pass a list of parameters that can be pruned to not start from scratch in case a tighter threshold should be probed

For example consider the following hypothetical call:

    gaga12.enablePruning("mu_XS_ggH,mu_XS_VBF,mu_XS_WH,mu_XS_ZH,mu_XS_ttH", "nui_ATLAS_*,nui_*", "1.0,1.0,1.0,1.0,1.0", "auto:5", 2)

The 1.0 is the weight which will be used to compute a weighted average of the provided POIs. The order in which the NPs are checked for pruning depends on the uncertainty on that weighted average, but the pruning itself does not, as this checks the individual POIs. 1.0 here means, that you don't bias your pruning to prefer pruning uncertainties associated to any production mode first. Imagine putting the XS as weight, this will give a higher weight on ggF, so that VBF related uncertainties are checked before for pruning.

auto:5 and 2 defines the threshold for pruning. auto tells the algorithm to base the decision on the actual precision quoted after rounding following the PDG rules. 5 is the threshold that the selected digit is allowed to vary and 2 defines the (non-)significant digit to check. So in this example the 2nd non-significant digit after rounding is allowed to vary by up to 5 units.

When running the pruning, the logfile will show explicitly what will happen, for example:

    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) Parsed POI mu_XS_ggH, assigning weight 1 and threshold auto:5
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) Parsed POI mu_XS_VBF, assigning weight 1 and threshold auto:5
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) Parsed POI mu_XS_WH, assigning weight 1 and threshold auto:5
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) Parsed POI mu_XS_ZH, assigning weight 1 and threshold auto:5
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) Parsed POI mu_XS_ttH, assigning weight 1 and threshold auto:5
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) Nuisance parameters will be pruned in the following order: nui_ATLAS_*; nui_*;
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) Harvest parameter (3): mu_XS_ggH with Hesse error 1.325 (1.32471) +/- 0.39 (0.390298)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) Harvest parameter (0): mu_XS_VBF with Hesse error 1.155 (1.15525) +/- 0.854 (0.853855)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) Harvest parameter (1): mu_XS_WH with Hesse error 1.172 (1.17236) +/- 1.743 (1.74297)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) Harvest parameter (2): mu_XS_ZH with Hesse error 0.266 (0.265748) +/- 3.507 (3.50656)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) Harvest parameter (4): mu_XS_ttH with Hesse error 0.644 (0.643915) +/- 1.701 (1.70077)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) Initial averaged POI with propagated uncertainties: 0.912397 +/- 0.663345

Note that the quoted result mu\_XS\_ggH should be 1.3 +/- 0.4

    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) --> Testing nui_ATLAS_stat_Cat10_2012_WH
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_VBF: absolute change 0.854 --> 0.854, position 1 of 146 (00.000000%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_WH: absolute change 1.743 --> 1.743, position 1 of 146 (00.000000%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_ZH: absolute change 3.507 --> 3.507, position 1 of 146 (00.000000%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_ggH: absolute change 0.39 --> 0.39, position 1 of 146 (00.000000%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_ttH: absolute change 1.701 --> 1.701, position 1 of 146 (00.000000%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  └-> Removing parameter!


This is a pruned NP which does not affect the uncertainty at all up the 2nd non-significant digit for any of the POIs considered.

    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) --> Testing nui_ATLAS_EL_SF
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_VBF: absolute change 0.854 --> 0.854, position 1 of 117 (-0.016513%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_WH: absolute change 1.743 --> 1.743, position 53 of 117 (-0.005773%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_ZH: absolute change 3.507 --> 3.506 tolerated: 3507 - 3506 < 5, position 41 of 117 (-0.010042%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_ggH: absolute change 0.39 --> 0.389 tolerated: 390 - 389 < 5, position 12 of 117 (-0.206217%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_ttH: absolute change 1.701 --> 1.7 tolerated: 1701 - 1700 < 5, position 23 of 117 (-0.016390%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  └-> Removing parameter!

This is a parameter that will be pruned because the 2nd non-significant digit is changed at most by only 1. Note that a change of 5 is tolerated as well and will result in removing the parameter.

    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12) --> Testing nui_ATLAS_LUMI_2012
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_VBF: absolute change 0.854 --> 0.848 reduced too much: 854 - 848 > 5, position 8 of 61 (-0.689649%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_WH: absolute change 1.743 --> 1.741 tolerated: 1743 - 1741 < 5, position 15 of 61 (-0.140544%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_ZH: absolute change 3.507 --> 3.503 tolerated: 3507 - 3503 < 5, position 7 of 61 (-0.110586%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_ggH: absolute change 0.39 --> 0.383 reduced too much: 390 - 383 > 5, position 15 of 61 (-1.807967%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  | mu_XS_ttH: absolute change 1.701 --> 1.697 tolerated: 1701 - 1697 < 5, position 8 of 61 (-0.244076%)
    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  └-> Vetoed parameter because uncertainty on one POI reduced too much!

This reduction is too much and the parameter will not be pruned. If more than 1 POI specified, all have to match the criteria. If one does not pass, the parameter is not pruned.

    [#1] INFO:ObjectHandling -- AbsMeasurement::PruneNuisanceParameters(gaga12)  └-> No more parameters found to prune!

In case no parameter is below your threshold the algorithm terminates.


<!-- Email addresses -->
[andrea]: mailto:andrea.gabrielli@cern.ch (Andrea Gabrielli)
[stefan]: mailto:stefan.gadatsch@cern.ch (Stefan Gadatsch)
[aaron]: mailto:aaron.james.armbruster@cern.ch (Aaron Armbruster)
[haoshuang]: mailto:haoshuang.ji@cern.ch (Haoshuang Ji)
<!-- Links -->
[svn]: https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/HiggsPhys/CombinationTools/RooStatTools/trunk/CombinationTool (Likelihood Combination Tool)
[readme]: https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/HiggsPhys/CombinationTools/RooStatTools/trunk/CombinationTool/Readme.md (README)
[example]: https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/HiggsPhys/CombinationTools/RooStatTools/trunk/CombinationTool/scripts/combine_lvlv.py (HWW Moriond 2013 combination)
[svn_oldtool]: https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/HiggsPhys/CombinationTools/RooStatTools/trunk/WorkspaceCombiner/ (Combination framework by Haoshuang Ji)
[hsg7root]: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HSG7RootBuild (ATLAS HSG7 ROOT build)
