#!/usr/bin/env python
""" Combination of measurements.

Builds a combined measurement from regularised channels.
"""

from __future__ import absolute_import

import logging
import os
import subprocess
import sys

import ROOT

from utils import *
from AbsMeasurement import AbsMeasurement


__author__ = "Stefan Gadatsch"
__credits__ = ["Stefan Gadatsch", "Andrea Gabrielli"]
__version__ = "0.1"
__maintainer__ = "Stefan Gadatsch"
__email__ = "stefan.gadatsch@cern.ch"

__all__ = [
    'CombinedMeasurement'
]


FORMAT = '%(asctime)s - %(processName)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
existing_logger = logging.getLogger('x')


class CombinedMeasurement(AbsMeasurement):
    def __init__(self, name, path=None, wsname=None, mcname=None, dataname=None):
        AbsMeasurement.__init__(self, name, path, wsname, mcname, dataname)

        self._measurements = dict()
        self._pois = ROOT.RooArgSet()
        self._obs = ROOT.RooArgSet()
        self._globs = ROOT.RooArgSet()
        self._nuis = ROOT.RooArgSet()
        self._asimov = None
        self._data = None

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Created CombinedMeasurement " + self._name)
        existing_logger.info(msg)

    def AddMeasurement(self, measurement):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Adding Measurement " + measurement.name)
        existing_logger.info(msg)
        self._measurements[measurement.name] = measurement

    @property
    def correlations(self):
        return self._correlations

    @correlations.setter
    def correlations(self, correlations):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Set correlation scheme " + correlations.name)
        existing_logger.info(msg)
        self._correlations = correlations

    def CollectMeasurements(self):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Collecting measurements")
        existing_logger.info(msg)
        tmpAllNuisanceParameters = dict()
        for meas in self._measurements:
            measurement = self._measurements[meas]
            measurement.initialise()
            thisNuisanceParameters = measurement.nuis
            tmpAllNuisanceParameters[measurement.name] = thisNuisanceParameters

        # TODO(Correlations) Automatically determine correlations

        for meas in self._measurements:
            measurement = self._measurements[meas]
            measurement.renamings = self._correlations.renamings[meas]
            measurement.CollectChannels()

            # TODO(Renamings) Propagate back the changes of the renamings

    def CombineMeasurements(self):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Combining measurements")
        existing_logger.info(msg)

        ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, RooDataSet*>", "map;string;RooDataSet.h")
        ROOT.gInterpreter.GenerateDictionary("std::map<std::string, RooDataSet*>", "map;string;RooDataSet.h")
        ROOT.gInterpreter.GenerateDictionary("std::pair<std::map<string,RooDataSet*>::iterator, bool>", "map;string;RooDataSet.h")

        ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, RooAbsPdf*>", "map;string;RooAbsPdf.h")
        ROOT.gInterpreter.GenerateDictionary("std::map<std::string, RooAbsPdf*>", "map;string;RooAbsPdf.h")
        ROOT.gInterpreter.GenerateDictionary("std::pair<std::map<string,RooAbsPdf*>::iterator, bool>", "map;string;RooAbsPdf.h")

        if sys.platform != "darwin":
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Generating necessary dictionaries for non-darwin platform")
            existing_logger.debug(msg)

            ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, RooDataSet*>", "map;string;RooDataSet.h")
            ROOT.gInterpreter.GenerateDictionary("std::map<std::string, RooDataSet*>", "map;string;RooDataSet.h")
            ROOT.gInterpreter.GenerateDictionary("std::pair<std::map<string,RooDataSet*>::iterator, bool>", "map;string;RooDataSet.h")

            ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, RooAbsPdf*>", "map;string;RooAbsPdf.h")
            ROOT.gInterpreter.GenerateDictionary("std::map<std::string, RooAbsPdf*>", "map;string;RooAbsPdf.h")
            ROOT.gInterpreter.GenerateDictionary("std::pair<std::map<string,RooAbsPdf*>::iterator, bool>", "map;string;RooAbsPdf.h")
        # else:
        #     msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Generating necessary dictionaries for Mac")
        #     existing_logger.debug(msg)
        #
        #     fileName = DumpCode('#include <map>\n#include <string>')
        #     command = 'root -l -q -b -e gSystem->CompileMacro(\"%s\",\"k\")*0' % fileName
        #     subprocess.check_output(command.split())
        #     libNameBase = fileName.replace(".C", "_C")
        #     ROOT.gSystem.Load(libNameBase)



        datasetMap = ROOT.std.map('string, RooDataSet*')()
        datasetMap.keepalive = list()
        pdfMap = ROOT.std.map('string, RooAbsPdf*')()
        pdfMap.keepalive = list()

        category = ROOT.RooCategory("master_measurement", "master_measurement")

        numTotPdf = 0

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Defining categories")
        existing_logger.info(msg)

        for meas in self._measurements:
            measurement = self._measurements[meas]
            channels = measurement.channels

            thisObservables = measurement.obs
            thisGlobalObservables = measurement.globs
            thisNuisanceParameters = measurement.nuis

            self._obs.add(thisObservables)
            self._globs.add(thisGlobalObservables)
            self._nuis.add(thisNuisanceParameters)
            self._globs.setAttribAll("GLOBAL_OBSERVABLE")

            for chan in channels:
                channel = channels[chan]
                msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Defining category " + repr(numTotPdf) + " with name " + channel.name)
                existing_logger.info(msg)
                pdf_tmp = channel.pdf
                data_tmp = channel.data
                category.defineType(channel.name, numTotPdf)
                category.setLabel(channel.name, True)
                pdfMap.insert(pdfMap.begin(), ROOT.std.pair("string,RooAbsPdf*")(channel.name, pdf_tmp))
                datasetMap.insert(datasetMap.begin(), ROOT.std.pair("string,RooDataSet*")(channel.name, data_tmp))
                numTotPdf += 1
        self._obs.add(category)

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Making combined pdf")
        existing_logger.info(msg)

        self._pdf = ROOT.RooSimultaneous("combPdf", "combPdf", pdfMap, category)
        self._pdf.setStringAttribute("DefaultGlobalObservablesTag", "GLOBAL_OBSERVABLE")

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Making combined dataset")
        existing_logger.info(msg)

        weightVar = ROOT.RooRealVar("weightVar2", "", 1.0, -1e10, 1e10)
        obs_cat_weight = ROOT.RooArgSet()
        obs_cat_weight.add(self._obs)
        obs_cat_weight.add(category)
        obs_cat_weight.add(weightVar)

        self._data = ROOT.RooDataSet("combData", "combData", obs_cat_weight, ROOT.RooFit.Index(category), ROOT.RooFit.Import(datasetMap), ROOT.RooFit.WeightVar("weightVar2"))

        self.MakeCleanWorkspace()

    def MakeCleanWorkspace(self):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Making a clean combined workspace")
        existing_logger.info(msg)

        self._pois.sort()
        self._nuis.sort()
        self._globs.sort()
        self._obs.sort()

        self._ws = ROOT.RooWorkspace("combined")
        self._ws.autoImportClassCode(True)
        self._mc = ROOT.RooStats.ModelConfig("ModelConfig", self._ws)

        tmpPdfName = self._pdf.GetName()
        ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.WARNING)
        getattr(self._ws, 'import')(self._pdf, ROOT.RooFit.RecycleConflictNodes(), ROOT.RooFit.Silence())
        ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.DEBUG)
        self._pdf = self._ws.pdf(tmpPdfName)

        getattr(self._ws, 'import')(self._data)
        if self._asimov is not None:
            getattr(self._ws, 'import')(self._asimov)

        self._mc.SetPdf(self._pdf)
        self.DefineParametersOfInterest(self._mc)
        self._mc.SetParametersOfInterest(self._pois)
        self._mc.SetNuisanceParameters(self._nuis)
        self._mc.SetObservables(self._obs)
        self._mc.SetGlobalObservables(self._globs)

        getattr(self._ws, 'import')(self._mc)

    def DefineParametersOfInterest(self, mc):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Identifying parameters of interest")
        existing_logger.info(msg)

        ws = mc.GetWorkspace()
        oldPois = self._pois
        self._pois.removeAll()

        pois = self._correlations.ParametersOfInterest.split(",")
        for poi in pois:
            if ws.var(poi):
                msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Adding " + poi + " to parameters of interest")
                existing_logger.debug(msg)
                self._pois.add(ws.var(poi))

                if self._nuis.find(ws.var(poi)):
                    msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Removing " + poi + " from nuisance parameters")
                    existing_logger.debug(msg)
                    self._nuis.remove(self._nuis.find(ws.var(poi)))
            else:
                msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Failed adding " + poi + " to parameters of interest. Not present in combined workspace.")
                existing_logger.warning(msg)

        poiIter = oldPois.createIterator()
        nextPoi = poiIter.Next()
        while nextPoi:
            if (ws.var(nextPoi.GetName()) and not self._pois.find(nextPoi)):
                msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Making old poi " + nextPoi.GetName() + " a nuisance parameter")
                existing_logger.debug(msg)
                self._nuis.add(nextPoi)
            nextPoi = poiIter.Next()

    def MakeAsimovData(self, Conditional, profileAt, generateAt):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Adding Asimov data")
        existing_logger.info(msg)

        self.MakeSnapshots(profileAt, Conditional)
        if generateAt is Snapshot.ucmles and generateAt is not Snapshot.profileAt:
            self.MakeSnapshots(generateAt, Conditional)

        mu = self._mc.GetParametersOfInterest().first()
        muVal = mu.getVal()
        if generateAt is Snapshot.background:
            muVal = 0
        elif generateAt is Snapshot.nominal:
            muVal = 1
        elif generateAt is Snapshot.ucmles:
            self._ws.loadSnapshot("ucmles")
            muVal = mu.getVal()
            self._ws.loadSnapshot("nominalNuis")
            self._ws.loadSnapshot("nominalGlobs")
        else:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Unknown value for generation requested.")
            existing_logger.error(msg)
            return

        muStr = ""
        if profileAt is Snapshot.background:
            muStr = "_0"
        elif profileAt is Snapshot.nominal:
            muStr = "_1"
        elif profileAt is Snapshot.ucmles:
            muStr = "_muhat"
        else:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Unknown value for profiling requested.")
            existing_logger.error(msg)
            return

        if not Conditional:
            self._ws.loadSnapshot("nominalGlobs")
            self._ws.loadSnapshot("nominalNuis")

        self._ws.loadSnapshot("nominalGlobs")
        self._ws.loadSnapshot("conditionalNuis" + muStr)

        mu.setVal(muVal)

        # TODO(Binning) Add option to modify binning of observables

        genPoiValues = self._pois.snapshot()
        allParams = self._mc.GetPdf().getParameters(self._data)
        ROOT.RooStats.RemoveConstantParameters(allParams)
        allParams = genPoiValues

        thisAsimovData = ROOT.RooStats.AsymptoticCalculator.MakeAsimovData(self._mc, allParams, self._globs)
        thisAsimovData.SetName("asimovData" + (muStr if profileAt is generateAt else ""))
        thisAsimovData.SetTitle("asimovData" + (muStr if profileAt is generateAt else ""))
        getattr(self._ws, 'import')(thisAsimovData)
        self._asimov = thisAsimovData

        self._ws.loadSnapshot("nominalNuis")
        self._ws.loadSnapshot("nominalGlobs")

    def MakeSnapshots(self, thisSnapshot, Conditional):
        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Making snapshot")
        existing_logger.info(msg)

        mu = self._mc.GetParametersOfInterest().first()
        muVal = mu.getVal()
        if thisSnapshot is Snapshot.background:
            muVal = 0
        elif thisSnapshot is Snapshot.nominal:
            muVal = 1
        elif thisSnapshot is Snapshot.ucmles:
            # Will be set later
            muVal = 1
        else:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Unknown snapshot requested.")
            existing_logger.error(msg)
            return

        mu.setVal(muVal)

        muStr = ""
        if thisSnapshot is Snapshot.background:
            muStr = "_0"
        elif thisSnapshot is Snapshot.nominal:
            muStr = "_1"
        elif thisSnapshot is Snapshot.ucmles:
            muStr = "_muhat"
        else:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Unknown snapshot requested.")
            existing_logger.error(msg)
            return

        mc_obs = self._mc.GetObservables()
        mc_globs = self._mc.GetGlobalObservables()
        mc_nuis = self._mc.GetNuisanceParameters()

        # Pair the nuisance parameters and global observables
        mc_nuis_tmp = mc_nuis
        nui_list = ROOT.RooArgList("ordered_nuis")
        glob_list = ROOT.RooArgList("ordered_globs")
        constraint_set_tmp = ROOT.RooArgSet(self._pdf.getAllConstraints(mc_obs, mc_nuis_tmp, False))
        constraint_set = ROOT.RooArgSet()

        UnfoldConstraints(constraint_set_tmp, constraint_set, mc_obs, mc_nuis_tmp)

        cIter = constraint_set.createIterator()
        arg = cIter.Next()
        while arg:
            thisNui = None
            nIter = mc_nuis.createIterator()
            nui_arg = nIter.Next()
            while nui_arg:
                if arg.dependsOn(nui_arg):
                    thisNui = nui_arg
                    break
                nui_arg = nIter.Next()

            components = arg.getComponents()
            components.remove(arg)
            if components.getSize():
                itr1 = components.createIterator()
                arg1 = itr1.Next()
                while arg1:
                    itr2 = components.createIterator()
                    arg2 = itr2.Next()
                    while arg2:
                        if arg1 == arg2:
                            arg2 = itr2.Next()
                            continue
                        if arg2.dependsOn(arg1):
                            components.remove(arg1)
                        arg2 = itr2.Next()
                    arg1 = itr1.Next()

            if components.getSize() > 1:
                msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Failed to isolate proper nuisance parameter")
                existing_logger.error(msg)
                return
            elif components.getSize() == 1:
                thisNui = components.first()

            thisGlob = None
            gIter = mc_globs.createIterator()
            glob_arg = gIter.Next()
            while glob_arg:
                if arg.dependsOn(glob_arg):
                    thisGlob = glob_arg
                    break
                glob_arg = gIter.Next()

            if (thisNui is None) or (thisGlob is None):
                msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Failed to find nuisance parameter or global observable for constraint " + arg.GetName())
                existing_logger.warning(msg)
                continue

            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Pairing nuisance parameter " + thisNui.GetName() + " with global observable " + thisGlob.GetName() + " from constraint " + arg.GetName())
            existing_logger.debug(msg)
            nui_list.add(thisNui)
            glob_list.add(thisGlob)

            arg = cIter.Next()

        if nui_list.getSize() != glob_list.getSize():
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Size does not match")
            existing_logger.error(msg)
            return

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Saving nominal snapshots")
        existing_logger.debug(msg)
        self._ws.saveSnapshot("nominalGlobs", self._mc.GetGlobalObservables())
        self._ws.saveSnapshot("nominalNuis", self._mc.GetNuisanceParameters())

        mu.setVal(muVal)
        if (thisSnapshot == Snapshot.background) or (thisSnapshot == Snapshot.nominal):
            mu.setConstant(True)
        else:
            mu.setConstant(False)

        if Conditional:
            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Performing conditional fit")
            existing_logger.info(msg)

            # TODO(PDF arguments) Add handle to set arguments on pdfs, e.g. turn off level 2 constant term optimization
            # TODO(ExtendendMinimizer) Switch to ExtendendMinimizer

            result = self._pdf.fitTo(self._data, ROOT.RooFit.Minimizer("Minuit2", "minimize"), ROOT.RooFit.Strategy(0), ROOT.RooFit.Constrain(self._nuis), ROOT.RooFit.GlobalObservables(self._globs), ROOT.RooFit.Hesse(False), ROOT.RooFit.Offset(True), ROOT.RooFit.Optimize(2))
            if result.status is not 0:
                msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Conditional fit failed")
                existing_logger.error(msg)

        mu.setConstant(False)

        for i in xrange(0, nui_list.getSize()):
            nui = nui_list.at(i)
            glob = glob_list.at(i)

            msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Setting global observable " + glob.GetName() + " (previous value: " + str(glob.getVal()) + ") to conditional value: " + str(nui.getVal()))
            existing_logger.debug(msg)

            glob.setVal(nui.getVal())

        msg = '[{}] [{}] {}'.format(os.getpid(), self._name, "Saving conditional snapshots")
        existing_logger.debug(msg)
        self._ws.saveSnapshot("conditionalGlobs" + muStr, self._mc.GetGlobalObservables())
        self._ws.saveSnapshot("conditionalNuis" + muStr, self._mc.GetNuisanceParameters())

        if thisSnapshot == Snapshot.ucmles:
            nuisAndPOI = self._mc.GetNuisanceParameters()
            nuisAndPOI.add(mu)
            self._ws.saveSnapshot("ucmles", nuisAndPOI)

        self._ws.loadSnapshot("nominalNuis")
        self._ws.loadSnapshot("nominalGlobs")


if __name__ == '__main__':
    pass
