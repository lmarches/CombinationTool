#!/usr/bin/env python
""" Common utilities.

"""

from __future__ import absolute_import

import hashlib
import logging
import os
import re

import ROOT


__author__ = "Stefan Gadatsch"
__credits__ = ["Stefan Gadatsch", "Andrea Gabrielli", "Tim Adye", "Hongtao Yang"]
__version__ = "0.1"
__maintainer__ = "Stefan Gadatsch"
__email__ = "stefan.gadatsch@cern.ch"


FORMAT = '%(asctime)s - %(processName)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
existing_logger = logging.getLogger('x')


def FindUniqueProdComponents(pdf, components):
    pdfList = pdf.pdfList()
    if pdfList.getSize() is 1:
        msg = '[{}] [{}] {}'.format(os.getpid(), pdfList.at(0).GetName(), pdfList.at(0).GetName() + " is fundamental")
        existing_logger.debug(msg)
        components.add(pdfList)
    else:
        pdfIter = pdfList.createIterator()
        nextPdf = pdfIter.Next()
        while nextPdf:
            if nextPdf.IsA() is not ROOT.RooProdPdf.Class():
                components.add(nextPdf)
                nextPdf = pdfIter.Next()
                continue
            FindUniqueProdComponents(nextPdf, components)
            nextPdf = pdfIter.Next()


def UnfoldConstraints(initial, final, obs, nuis):
    itr = initial.createIterator()
    thisPdf = itr.Next()
    while thisPdf:
        nuis_tmp = nuis
        constraint_set = thisPdf.getAllConstraints(obs, nuis_tmp, False)
        if (thisPdf.IsA() is not ROOT.RooGaussian.Class()) and (thisPdf.IsA() is not ROOT.RooLognormal.Class()) and (thisPdf.IsA() is not ROOT.RooGamma.Class()) and (thisPdf.IsA() is not ROOT.RooPoisson.Class()) and (thisPdf.IsA() is not ROOT.RooBifurGauss.Class()):
            UnfoldConstraints(constraint_set, final, obs, nuis)
        else:
            final.add(thisPdf)
        thisPdf = itr.Next()


def RebinDataset(pdfIn, data, setNbins, generateBinnedTag, binnedCategories, unbinnedCategories, weightVarName):
    pdf = pdfIn
    cat = pdf.indexCat()
    dataList = data.split(cat, True)

    ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, RooDataSet*>", "map;string;RooDataSet.h")
    ROOT.gInterpreter.GenerateDictionary("std::map<std::string, RooDataSet*>", "map;string;RooDataSet.h")
    ROOT.gInterpreter.GenerateDictionary("std::pair<std::map<string,RooDataSet*>::iterator, bool>", "map;string;RooDataSet.h")

    dataMap = ROOT.std.map('string, RooDataSet*')()
    dataMap.keepalive = list()

    weightVar = ROOT.RooRealVar(weightVarName, "", 1.0)

    dsBinned = 0

    nrEntries = dataList.GetEntries()
    for idata in xrange(0, nrEntries):
        datai = dataList.At(idata)
        copyName = datai.GetName() + "_unbinned"
        obsPlusWeight = ROOT.RooArgSet()
        obsPlusWeight.add(datai.get())
        obsPlusWeight.add(weightVar)
        copyData = ROOT.RooDataSet(copyName, copyName, obsPlusWeight, weightVar.GetName())
        copyData.append(datai)
        dataMap.insert(dataMap.begin(), ROOT.std.pair("string,RooDataSet*")(datai.GetName(), copyData))

        isBinned = -1
        dataType = ""

        pdfi = pdf.getPdf(datai.GetName())
        obs = pdfi.getObservables(datai)

        if pdfi.isBinnedDistribution(obs):
            isBinned = 1
            dataType = " binned"
        else:
            isBinned = 0
            dataType = " unbinned"

        msg = '[{}] [{}] {}'.format(os.getpid(), data.GetName(), "Category " + datai.GetName() + dataType + " dataset has " + str(datai.numEntries()) + "/" + str(datai.sumEntries()) + " entries")
        existing_logger.debug(msg)

        if (datai.numEntries() <= 0):
            continue

        rebin = False
        genBinOnly = False

        if isBinned == 1 and re.search(unbinnedCategories, datai.GetName()):
            if datai.numEntries() <= datai.sumEntries():
                continue
            rebin = True
        elif isBinned == 0 and re.search(binnedCategories, datai.GetName()):
            if setNbins <= 0:
                continue
            genBinOnly = (datai.numEntries() <= setNbins)
            if genBinOnly and datai.sumEntries() <= setNbins:
                continue
        else:
            continue

        pdfi = pdf.getPdf(datai.GetName())
        obs = pdfi.getObservables(datai)

        if (not rebin) and (generateBinnedTag is not ""):
            pdfi.setAttribute(generateBinnedTag)

        msg = '[{}] [{}] {}'.format(os.getpid(), data.GetName(), ("Prune" if rebin else ("Generatet" if genBinOnly else "Set")) + " binning on" + dataType + " dataset " + datai.GetName() + " with " + str(datai.numEntries()) + "/" + str(datai.sumEntries()) + " entries and PDF " + pdfi.GetName())
        existing_logger.debug(msg)

        it = obs.createIterator()
        o = it.Next()
        while o:
            if not rebin:
                o.setBinning(ROOT.RooBinning(setNbins, o.getMin(), o.getMax()))
            o = it.Next()

        if genBinOnly:
            continue

        if obs.getSize() != 1 and not rebin:
            msg = '[{}] [{}] {}'.format(os.getpid(), data.GetName(), "PDF " + pdfi.GetName() + " has " + str(obs.getSize()) + " observables. Can't create binned dataset")
            existing_logger.warning(msg)
            continue

        obsVar = obs.first()
        newName = datai.GetName() + "_binned"
        obsPlusWeight = ROOT.RooArgSet()
        obsPlusWeight.add(obs)
        obsPlusWeight.add(weightVar)
        dataiNew = ROOT.RooDataSet(newName, newName, obsPlusWeight, weightVar.GetName())

        if rebin:
            binning = obsVar.getBinning()
            msg = '[{}] [{}] {}'.format(os.getpid(), data.GetName(), "Observable " + obsVar.GetName() + " bins (" + str(binning.numBins()) + "," + str(binning.lowBound()) + "," + str(binning.highBound()) + ") contain " + str(datai.numEntries()) + "/" + str(datai.sumEntries()) + "entries:")
            existing_logger.warning(msg)

            for j in xrange(0, datai.numEntries()):
                row = datai.get(j)
                w = datai.weight()
                if w == 0:
                    continue
                msg = '[{}] [{}] {}'.format(os.getpid(), data.GetName(), str(row.find(obsVar).getVal()) + ": " + str(w))
                existing_logger.warning(msg)
                dataiNew.add(row, w)

            if dataiNew.numEntries() == 0:
                dataiNew.add(datai.get(0), 0.0)

            if dataiNew.numEntries() >= datai.numEntries():
                continue

            msg = '[{}] [{}] {}'.format(os.getpid(), data.GetName(), "Rebin dataset from " + str(datai.numEntries()) + "/" + str(datai.sumEntries()) + " to " + str(dataiNew.numEntries()) + "/" + str(dataiNew.sumEntries()) + " in " + dataiNew.GetName())
            existing_logger.info(msg)

        else:
            histName = datai.GetName() + "_hist"
            hist = ROOT.RooAbsData.createHistogram(datai, histName, obsVar)

            msg = '[{}] [{}] {}'.format(os.getpid(), data.GetName(), "Created histogram " + hist.GetName() + "(" + str(hist.GetNbinsX()) + "," + str(hist.GetXaxis().GetXmin()) + "," + str(hist.GetXaxis().GetXmax()) + ")")
            existing_logger.info(msg)

            for j in xrange(1, hist.GetNbinsX() + 1):
                x = hist.GetXaxis().GetBinCenter(j)
                y = hist.GetBinContent(j)

                if y == 0.0:
                    continue

                obsVar.setVal(x)
                dataiNew.add(obs, y)

            if dataiNew.numEntries() == 0:
                obsVar.setVal(hist.GetXaxis().GetBinCenter(1))
                dataiNew.add(obs, 0.0)

            msg = '[{}] [{}] {}'.format(os.getpid(), data.GetName(), "In dataset " + dataiNew.GetName() + " with " + str(dataiNew.numEntries()) + "/" + str(dataiNew.sumEntries()) + " entries")
            existing_logger.debug(msg)

        dataMap.insert(dataMap.begin(), ROOT.std.pair("string,RooDataSet*")(datai.GetName(), dataiNew))
        dsBinned += 1

    if dsBinned <= 0:
        return None

    catVar = data.get().find(cat)
    newObs = ROOT.RooArgSet()
    allObs = pdf.getObservables(data)

    it = data.get().fwdIterator()
    dsvar = it.next()
    while dsvar:
        if allObs.find(dsvar):
            newObs.add(allObs.find(dsvar))
        dsvar = it.next()

    name = data.GetName() + "_binned"
    obsPlusWeight = ROOT.RooArgSet()
    obsPlusWeight.add(newObs)
    obsPlusWeight.add(weightVar)
    newData = ROOT.RooDataSet(name, name, obsPlusWeight, ROOT.RooFit.Index(catVar), ROOT.RooFit.Import(dataMap), ROOT.RooFit.WeightVar(weightVar))

    msg = '[{}] [{}] {}'.format(os.getpid(), data.GetName(), "Replace dataset " + data.GetName() + " (" + str(data.numEntries()) + "/" + str(data.sumEntries()) + " entries) with dataset " + newData.GetName() + " (" + str(newData.numEntries()) + "/" + str(newData.sumEntries()) + " entries)")
    existing_logger.info(msg)

    return newData


def DataHist2DataSet(pdfIn, dataIn):
    cat = pdfIn.indexCat()
    dataList = dataIn.split(cat, True)
    numChannels = dataList.GetEntries()

    ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, RooDataSet*>", "map;string;RooDataSet.h")
    ROOT.gInterpreter.GenerateDictionary("std::map<std::string, RooDataSet*>", "map;string;RooDataSet.h")
    ROOT.gInterpreter.GenerateDictionary("std::pair<std::map<string,RooDataSet*>::iterator, bool>", "map;string;RooDataSet.h")

    dataMap = ROOT.std.map('string, RooDataSet*')()
    dataMap.keepalive = list()

    x = list()
    w = list()
    data = list()

    Observables = ROOT.RooArgSet()

    for ich in xrange(0, numChannels):
        cat.setBin(ich)
        channelname = cat.getLabel()
        pdfi = pdfIn.getPdf(channelname)
        datai = dataList.At(ich)
        obsi = pdfi.getObservables(datai).first()

        x.append(obsi)
        w.append(ROOT.RooRealVar("weight_" + str(ich), "weight_" + str(ich), 1.0))

        args = ROOT.RooArgSet()
        args.add(ROOT.RooArgSet(x[ich], w[ich]))

        data.append(ROOT.RooDataSet("combData", "combData", args, ROOT.RooFit.WeightVar(w[ich])))

        obs_tmp = datai.get()
        xdata_tmp = obs_tmp.find(obsi.GetName())

        for ievt in xrange(0, datai.numEntries()):
            datai.get(ievt)
            weight = datai.weight()
            x[ich].setVal(xdata_tmp.getVal())
            w[ich].setVal(weight)
            data[ich].add(ROOT.RooArgSet(x[ich], w[ich]), weight)

        Observables.add(x[ich])
        dataMap.insert(dataMap.begin(), ROOT.std.pair("string,RooDataSet*")(channelname, data[ich]))

    weight = ROOT.RooRealVar("weight", "weight", 1.0)
    obsAndWeight = ROOT.RooArgSet()
    obsAndWeight.add(Observables)
    obsAndWeight.add(weight)
    newData = ROOT.RooDataSet("combData", "combData", obsAndWeight, ROOT.RooFit.Index(cat), ROOT.RooFit.Import(dataMap), ROOT.RooFit.WeightVar(weight))

    msg = '[{}] [{}] {}'.format(os.getpid(), dataIn.GetName(), "Replace datahist " + dataIn.GetName() + " with dataset " + newData.GetName())
    existing_logger.info(msg)

    return newData


def AddGhostEvents(dataIn, pdfIn, observables, globalPOI):
    max_soverb = 0
    mu_min = -10e9
    cat = pdfIn.indexCat()
    dataList = dataIn.split(cat, True)
    nrEntries = dataList.GetEntries()
    weightVar = ROOT.RooRealVar("weightVar", "weightVar", 1.0)

    ROOT.gInterpreter.GenerateDictionary("std::pair<std::string, RooDataSet*>", "map;string;RooDataSet.h")
    ROOT.gInterpreter.GenerateDictionary("std::map<std::string, RooDataSet*>", "map;string;RooDataSet.h")
    ROOT.gInterpreter.GenerateDictionary("std::pair<std::map<string,RooDataSet*>::iterator, bool>", "map;string;RooDataSet.h")

    dataMap = ROOT.std.map('string, RooDataSet*')()
    dataMap.keepalive = list()

    globalPOI.setVal(0)

    for itrChan in xrange(0, nrEntries):
        ds = dataList.At(itrChan)
        typeName = ds.GetName()
        cat.setLabel(typeName)
        pdf = pdfIn.getPdf(typeName)
        obs = pdf.getObservables(observables)

        obsAndWeightAndCat = ROOT.RooArgSet()
        obsAndWeightAndCat.add(obs)
        obsAndWeightAndCat.add(weightVar)
        obsAndWeightAndCat.add(cat)

        dataName = "newData_" + typeName
        thisData = ROOT.RooDataSet(dataName, dataName, obsAndWeightAndCat, ROOT.RooFit.WeightVar(weightVar))
        firstObs = obs.first()

        nrEntries = ds.numEntries()

        globalPOI.setVal(0)
        expB = pdf.expectedEvents(ROOT.RooArgSet(firstObs))

        globalPOI.setVal(1)
        expSB = pdf.expectedEvents(ROOT.RooArgSet(firstObs))

        globalPOI.setVal(0)
        vec_b = list()
        for ib in xrange(0, nrEntries):
            event = ds.get(ib)
            thisObs = event.find(firstObs.GetName())
            firstObs.setVal(thisObs.getVal())
            vec_b.append(expB * pdf.getVal(obs))

        globalPOI.setVal(1)
        vec_s = list()
        for ib in xrange(0, nrEntries):
            event = ds.get(ib)
            thisObs = event.find(firstObs.GetName())
            firstObs.setVal(thisObs.getVal())
            vec_s.append(expSB * pdf.getVal(obs) - vec_b[ib])

        for ib in xrange(0, nrEntries):
            event = ds.get(ib)
            thisObs = event.find(firstObs.GetName())
            firstObs.setVal(thisObs.getVal())

            b = vec_b[ib]
            s = vec_s[ib]

            if s > 0.0:
                mu_min = max(mu_min, -b/s)
                soverb = s / b

                if (soverb > max_soverb):
                    max_soverb = soverb
                    msg = '[{}] [{}] {}'.format(os.getpid(), dataIn.GetName(), "Found new maximum signal over background: " + str(soverb) + " in PDF " + pdf.GetName() + " at observable = " + str(thisObs.getVal()))
                    existing_logger.debug(msg)

            if b == 0.0 and s != 0.0:
                msg = '[{}] [{}] {}'.format(os.getpid(), dataIn.GetName(), "Expecting non-zero signal but zero background at observable = " + str(firstObs.getVal()) + " in PDF " + pdf.GetName())
                existing_logger.warning(msg)

            if s+b <= 0.0:
                msg = '[{}] [{}] {}'.format(os.getpid(), dataIn.GetName(), "Expecting zero events for observable = " + str(firstObs.setVal(thisObs.getVal())))
                existing_logger.warning(msg)
                continue

            weight = ds.weight()
            if weight == 0.0:
                msg = '[{}] [{}] {}'.format(os.getpid(), dataIn.GetName(), "Adding ghost event at observable = " + str(firstObs.getVal()))
                existing_logger.warning(msg)
                thisData.add(event, pow(10., -9.))
            else:
                thisData.add(event, weight)

        dataMap.insert(dataMap.begin(), ROOT.std.pair("string,RooDataSet*")(ds.GetName(), thisData))

    obsAndWeight = ROOT.RooArgSet()
    obsAndWeight.add(observables)
    obsAndWeight.add(weightVar)
    newData = ROOT.RooDataSet("combData", "combData", obsAndWeight, ROOT.RooFit.Index(cat), ROOT.RooFit.Import(dataMap), ROOT.RooFit.WeightVar(weightVar))

    msg = '[{}] [{}] {}'.format(os.getpid(), dataIn.GetName(), "Replace data " + dataIn.GetName() + " with data including ghost events " + newData.GetName())
    existing_logger.info(msg)

    return newData


def DumpCode(code):
    name = hashlib.sha1(code).hexdigest()[0:8] + ".C"
    with open(name, 'w') as file:
        file.write(code)

    return name


class Snapshot(object):
    background = 0
    nominal = 1
    ucmles = 2


if __name__ == '__main__':
    pass
