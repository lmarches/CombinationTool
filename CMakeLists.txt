cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
project(CombinationTool CXX)
set(CMAKE_BUILD_TYPE Release)

message("Project dir is ${PROJECT_SOURCE_DIR}")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/lib)

include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG(-std=c++11 COMPILER_SUPPORTS_CXX11)
if(COMPILER_SUPPORTS_CXX11)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
  add_definitions(-std=c++11)
else()
  message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2")
SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O2")

# Additional modules
set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/Modules" ${CMAKE_MODULE_PATH})
set(CMAKE_MODULE_PATH "$ENV{ROOTSYS}/etc/cmake/" ${CMAKE_MODULE_PATH})
set(CMAKE_MODULE_PATH "$ENV{ROOTSYS}/../src/cmake/modules/" ${CMAKE_MODULE_PATH})

# CERN ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
find_package(ROOT REQUIRED COMPONENTS Core Gpad Hist HistFactory MathCore Matrix Minuit Net Physics RIO Rint RooFit RooFitCore RooStats TMVA Thread Tree TreePlayer Proof ProofPlayer ProofBench ProofDraw)

# Includes
include(${ROOT_USE_FILE})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/inc)

# Generate dictionary
ROOT_GENERATE_DICTIONARY(G__CombinationTool inc/RenamingMap.h inc/ParametrisationSequence.h inc/ParametrisationScheme.h inc/Measurement.h inc/ExtendedMinimizer.h inc/CorrelationScheme.h inc/CombinedMeasurement.h inc/Channel.h inc/AbsMeasurement.h inc/WildcardList.h LINKDEF LinkDef.h)

# Shared library with generated dictionary
set(PROJECT_LIBRARIES ${ROOT_LIBRARIES})
add_library(CombinationTool SHARED src/AbsMeasurement.cxx src/Channel.cxx src/CombinedMeasurement.cxx src/CorrelationScheme.cxx src/ExtendedMinimizer.cxx src/Measurement.cxx src/ParametrisationScheme.cxx src/ParametrisationSequence.cxx src/RenamingMap.cxx G__CombinationTool.cxx )
target_link_libraries(CombinationTool ${PROJECT_LIBRARIES})